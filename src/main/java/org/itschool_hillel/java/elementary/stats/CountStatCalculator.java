package org.itschool_hillel.java.elementary.stats;

import java.util.Collection;
import java.util.Set;

public interface CountStatCalculator {

    /**
     * Метод принимает коллекцию строк. В этой коллекции отыскиваются два
     * наиболее часто встречающихся значения. Из этих значений формируется
     * множество, которое возвращается клиенту.
     *
     * @param input
     *      Коллекция строк
     *
     * @return
     *      Множество наиболее часто встречающихся строк. Величина множества
     *      не должна превышать 2.
     */
    Set<String> getTwoMostFrequent(Collection<String> input);

    /**
     * Метод принимает коллекцию строк. В этой коллекции отыскиваются два
     * наиболее редко встречающихся значения. Из этих значений формируется
     * множество, которое возвращается клиенту.
     *
     * @param input
     *      Коллекция строк
     *
     * @return
     *      Множество наиболее редко встречающихся строк. Величина множества
     *      не должна превышать 2.
     */
    Set<String> getTwoLeastFrequent(Collection<String> input);
}
