package org.itschool_hillel.java.elementary.stats;

import java.util.Collection;
import java.util.Set;

public class CountStatCalculatorImpl implements CountStatCalculator {

    @Override
    public Set<String> getTwoMostFrequent(Collection<String> input) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Set<String> getTwoLeastFrequent(Collection<String> input) {
        throw new UnsupportedOperationException();
    }
}
