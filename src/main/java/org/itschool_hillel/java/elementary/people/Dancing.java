package org.itschool_hillel.java.elementary.people;

public interface Dancing {
    String dance() throws StomachOverflowException;
}
