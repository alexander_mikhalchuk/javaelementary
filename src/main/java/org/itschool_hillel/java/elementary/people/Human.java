package org.itschool_hillel.java.elementary.people;

import org.itschool_hillel.java.elementary.fastfood.Dish;

import java.util.List;

public class Human {
    protected String name;
    protected Sex sex;
    protected String stomach;

    public Human(String name, Sex sex) {
        this.name = name;
        this.sex = sex;
    }

    public String getName() {
        return name;
    }

    public void eat(String food) throws StomachOverflowException {
    }

    @Deprecated
    public void smoke() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Human)) return false;

        Human human = (Human) o;

        if (name != null ? !name.equals(human.name) : human.name != null) return false;
        if (sex != human.sex) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (sex != null ? sex.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return name;
    }

    public void eat(List<Dish> dishes) {

    }
}
