package org.itschool_hillel.java.elementary.people;

public enum Sex {
    MAN, WOMAN
}
