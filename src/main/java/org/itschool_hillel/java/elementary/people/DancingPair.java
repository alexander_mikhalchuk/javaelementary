package org.itschool_hillel.java.elementary.people;


public class DancingPair<M extends Human, W extends Human> extends Pair<M, W> implements Dancing {
    public DancingPair(M first, W second) {
        super(first, second);
    }

    @Override
    public String dance() throws StomachOverflowException {
        first.eat("apple");
        second.eat("apple");
        String dance = first + " и " + second + " танцуют вальс.";
        System.out.println(dance);
        return dance;
    }
}
