package org.itschool_hillel.java.elementary.people;

public class Woman extends Human {
    public Woman(String name) {
        super(name, Sex.WOMAN);
    }

    public Human born(Man father) {
        if (System.currentTimeMillis() % 2 == 1) {
            return new Man(this.name + " " + father.name);
        } else {
            return new Woman(father.name + " " + this.name);
        }
    }
}
