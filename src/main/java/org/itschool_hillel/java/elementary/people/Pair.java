package org.itschool_hillel.java.elementary.people;


public class Pair<M, W> {
    protected M first;
    protected W second;

    public Pair(M first, W second) {
        this.first = first;
        this.second = second;
    }
}
