package org.itschool_hillel.java.elementary.thread;

public class ThreadDemo {
    public static void main(String args[]) {
        Thread thread1 = new Thread(new RunnableDemo("Thread-1"));
        thread1.start();
        Thread thread2 = new Thread(new RunnableDemo("Thread-2"));
        thread2.start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                // do some work
            }
        }).start();
    }
}