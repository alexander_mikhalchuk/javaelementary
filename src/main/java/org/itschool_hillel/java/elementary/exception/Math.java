package org.itschool_hillel.java.elementary.exception;

public class Math {
    public static int avg(int[] a) {
        return sum(a) / a.length;
    }

    public static int sum(int[] a) {
        int s = 0;
        for(int d: a) {
            s += d;
        }
        return s;
    }
}
