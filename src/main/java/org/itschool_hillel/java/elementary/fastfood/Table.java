package org.itschool_hillel.java.elementary.fastfood;

import org.itschool_hillel.java.elementary.people.Human;

public class Table {
    private Human client;

    public void setClient(Human client) {
        this.client = client;
    }

    public boolean isFree() {
        return client == null;
    }
}
