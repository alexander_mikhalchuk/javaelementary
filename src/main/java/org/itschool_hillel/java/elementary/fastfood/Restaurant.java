package org.itschool_hillel.java.elementary.fastfood;

import org.itschool_hillel.java.elementary.people.Human;

import java.util.*;

public class Restaurant {
    private Set<Dish> menu = new HashSet<Dish>();
    private List<Table> tables = new ArrayList<Table>();
    private Set<Waiter> waiters = new TreeSet<Waiter>();

    public void addDish(Dish dish) {
        menu.add(dish);
    }

    public void addTable(Table table) {
        tables.add(table);
    }

    public void addWaiter(Waiter waiter) {
        waiters.add(waiter);
    }

    public boolean serve(Human client, List<Dish> dishes) {
        Table freeTable = findFreeTable();
        if (freeTable != null) {
            freeTable.setClient(client);
            if (menuContains(dishes)) {
                client.eat(dishes);
                return true;
            }
        }
        return false;
    }

    private boolean menuContains(List<Dish> dishes) {
        return false;
    }

    private Table findFreeTable() {
        return null;
    }
}
