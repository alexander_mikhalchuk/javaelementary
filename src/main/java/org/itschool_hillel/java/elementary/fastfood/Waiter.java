package org.itschool_hillel.java.elementary.fastfood;

import org.itschool_hillel.java.elementary.people.Human;

public class Waiter implements Comparable<Waiter> {
    private final Human human;

    public Waiter(Human human) {
        this.human = human;
    }

    @Override
    public int compareTo(Waiter o) {
        return human.getName().compareTo(o.human.getName());
    }
}
