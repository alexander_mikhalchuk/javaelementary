package org.itschool_hillel.java.elementary;

public class Point {
    public static final Point ORIGIN;

    private int x;
    private int y;

    static {
        ORIGIN = new Point(0, 0);
    }

    public static Point newInstance(int x, int y) {
        return new Point(x, y);
    }

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public double distance(Point p) {
        double deltaX = this.x - p.x;
        double deltaY = this.y - p.y;

        return Math.sqrt(deltaX * deltaX + deltaY * deltaY);
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
