package org.itschool_hillel.java.elementary.wash;

public class Cat implements Washable {
    public void wash() {
        System.out.println("Кошка помылась.");
    }

    public String toString() {
        return "Кот";
    }
}
