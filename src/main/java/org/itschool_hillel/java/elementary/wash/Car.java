package org.itschool_hillel.java.elementary.wash;

public class Car implements Washable {
    @Override
    public void wash() {
        // Бесконтактная мойка
        System.out.println("Меня помыли.");
    }
}
