package org.itschool_hillel.java.elementary.wash;

public interface Washable {
    void wash();
}
