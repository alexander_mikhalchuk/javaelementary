package org.itschool_hillel.java.elementary;

import org.junit.Assert;
import org.junit.Test;

public class ParameterTest {

    void m1(int x) {
        x = 100;
    }

    @Test
    public void testIntParameter() {
        int x = 1;
        m1(x);
        Assert.assertEquals(1, x);
    }

    void m2(Integer x) {
        x = 100;
    }

    @Test
    public void testIntegerParameter() {
        Integer x = 1;
        m1(x);
        Assert.assertEquals(new Integer(1), x);
    }

    void m3(Point p) {
        p.setX(100);
    }

    @Test
    public void testPointParameter() {
        Point p = new Point(1, 3);
        m3(p);
        Assert.assertEquals(100, p.getX());
    }

    void zero(Point p) {
        p = new Point(0, 0);
    }

    void zero1(final Point p) {
        // The next line is an error.
        // p = new Point(0, 0);
    }

    @Test
    public void testZero() {
        Point p = new Point(1, 2);
        zero(p);
        Assert.assertEquals(1, p.getX());
        Assert.assertEquals(2, p.getY());
    }
}
