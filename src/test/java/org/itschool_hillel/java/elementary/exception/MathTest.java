package org.itschool_hillel.java.elementary.exception;

import org.junit.Assert;
import org.junit.Test;

public class MathTest {
    @Test
    public void testAvgEmptyArray() {
        try {
            org.itschool_hillel.java.elementary.exception.Math.avg(new int[]{});
            Assert.fail("Ожидался ArithmeticException");
        } catch (ArithmeticException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    @Test(expected = ArithmeticException.class)
    public void testAvgEmptyArray2() {
        Math.avg(new int[]{});
    }
}
