package org.itschool_hillel.java.elementary.exception;

import org.itschool_hillel.java.elementary.people.Man;
import org.itschool_hillel.java.elementary.people.StomachOverflowException;
import org.junit.Assert;
import org.junit.Test;

public class EatTest {
    @Test
    public void testEatOneTime() {
        try {
            new Man("Chuck Norris").eat("food");
        } catch (StomachOverflowException e) {
            Assert.fail("Не может быть переполнения желудка, когда ешь один раз");
        }
    }

    @Test(expected = StomachOverflowException.class)
    public void testEatTwoTimes() throws StomachOverflowException {
        Man chuck = new Man("Chuck Norris");
        chuck.eat("food");
        chuck.eat("more food");
        Assert.fail("Много еды должно вызвать переполнения желудка");
    }

    @Test
    public void testEatTwoTimesWithMezim() {
        Man chuck = new Man("Chuck Norris");
        try {
            chuck.eat("food");
            chuck.eat("мезим");
            chuck.eat("more food");
        } catch (StomachOverflowException e) {
            Assert.fail("Мезим не помог! Как?!");
        }
    }
}
