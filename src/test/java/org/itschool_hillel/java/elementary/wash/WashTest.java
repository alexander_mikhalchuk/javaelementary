package org.itschool_hillel.java.elementary.wash;

import org.junit.Test;

public class WashTest {
    @Test
    public void testWash() {
        Washable something = new Washable() {
            @Override
            public void wash() {
                System.out.println("Нечто, что может умываться, умылось.");
            }
        };

        Washable[] humans = {
                new Cat(),
                something,
                new Car()};

        for (int i = 0; i < humans.length; i++) {
            humans[i].wash();
        }
    }
}
