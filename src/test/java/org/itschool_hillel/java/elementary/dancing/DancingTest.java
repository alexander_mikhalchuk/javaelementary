package org.itschool_hillel.java.elementary.dancing;

import org.itschool_hillel.java.elementary.people.DancingPair;
import org.itschool_hillel.java.elementary.people.Man;
import org.itschool_hillel.java.elementary.people.StomachOverflowException;
import org.itschool_hillel.java.elementary.people.Woman;
import org.junit.Assert;
import org.junit.Test;

public class DancingTest {
    @Test
    public void testDancingPeople() throws StomachOverflowException {
        DancingPair<Man, Woman> pair =
                new DancingPair<Man, Woman>(new Man("John"), new Woman("Rebecka"));
        Assert.assertEquals("John и Rebecka танцуют вальс.", pair.dance());
    }

    @Test
    public void testDancingCats() throws StomachOverflowException {
        DancingPair<Man, Man> pair =
                new DancingPair<Man, Man>(new Man("Chuck"), new Man("Arnold"));
        Assert.assertEquals("Chuck и Arnold танцуют вальс.", pair.dance());
    }
}
