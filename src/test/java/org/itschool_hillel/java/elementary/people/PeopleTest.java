package org.itschool_hillel.java.elementary.people;

import org.junit.Assert;
import org.junit.Test;

public class PeopleTest {

    @Test
    public void testHumanConstructor() throws StomachOverflowException {
        Woman eva = new Woman("Eva");
        Man adam = new Man("Adam");

        Assert.assertEquals(Sex.MAN, adam.sex);
        Assert.assertEquals(Sex.WOMAN, eva.sex);

        adam.eat("una manzana");
        eva.eat("an apple");

        adam.smoke();

        Human baby = eva.born(adam);

        if (baby instanceof Man) {
            Assert.assertEquals(eva.name + " " + adam.name, baby.name);
        }

        if (baby instanceof Woman) {
            Assert.assertEquals(adam.name + " " + eva.name, baby.name);
        }
    }

    @Test
    public void testHumanEquals() {
        Human human1 = new Human("john", Sex.MAN);
        Human human2 = new Human("john", Sex.MAN);

        Assert.assertFalse(human1 == human2);
        Assert.assertEquals(human1, human2);
    }

    @Test
    public void testEqualsSelf() {
        Human human = new Human("Chuck Norris", Sex.MAN);

        boolean result = human.equals(human);
        Assert.assertTrue(result);
    }

    @Test
    public void testEqualsSimmetricTrue() {
        Human chuck1 = new Human("Chuck Norris", Sex.MAN);
        Human chuck2 = new Human("Chuck Norris", Sex.MAN);

        Assert.assertTrue(chuck1.equals(chuck2));
        Assert.assertTrue(chuck2.equals(chuck1));
    }

    @Test
    public void testEqualsSimmetricFalse() {
        Human chuck = new Human("Chuck Norris", Sex.MAN);
        Human buck = new Human("Buck Smith", Sex.MAN);

        Assert.assertFalse(chuck.equals(buck));
        Assert.assertFalse(buck.equals(chuck));
    }

    @Test
    public void testEqualsTransitive() {
        Human chuck1 = new Human("Chuck Norris", Sex.MAN);
        Human chuck2 = new Human("Chuck Norris", Sex.MAN);
        Human chuck3 = new Human("Chuck Norris", Sex.MAN);

        Assert.assertTrue(chuck1.equals(chuck2));
        Assert.assertTrue(chuck2.equals(chuck3));
        Assert.assertTrue(chuck1.equals(chuck3));
    }

    @Test
    public void testEqualsConsistentTrue() {
        Human chuck1 = new Human("Chuck Norris", Sex.MAN);
        Human chuck2 = new Human("Chuck Norris", Sex.MAN);

        boolean result = chuck1.equals(chuck2);

        for (int i = 0; i < 100; i++) {
            Assert.assertEquals(result, chuck1.equals(chuck2));
        }
    }

    @Test
    public void testEqualsConsistentFalse() {
        Human chuck1 = new Human("Chuck Norris", Sex.MAN);
        Human buck = new Human("Buck Smith", Sex.MAN);

        boolean result = chuck1.equals(buck);

        for (int i = 0; i < 100; i++) {
            Assert.assertEquals(result, chuck1.equals(buck));
        }
    }

    @Test
    public void testEqualsNull() {
        Human chuck = new Human("Chuck Norris", Sex.MAN);
        Assert.assertFalse(chuck.equals(null));
    }

    @Test
    public void testEqualsIncompatibleTypes() {
        Human chuck = new Human("Chuck Norris", Sex.MAN);
        Assert.assertFalse(chuck.equals("the string"));
    }

    @Test
    public void testEqualsCompatibleType() {
        Human chuck1 = new Human("Chuck Norris", Sex.MAN);
        Man chuck2 = new Man("Chuck Norris");

        Assert.assertTrue(chuck1.equals(chuck2));
    }
}
