package org.itschool_hillel.java.elementary;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CollectionTest {
    @Test
    public void testArrayList() {
        List<String> list = new ArrayList<String>(10);
        Assert.assertEquals(0, list.size());
    }

    @Test
    public void testMap() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("white", "#FFFFFF");
        map.put("black", "#000000");
        map.put("red", "#FF0000");
        for (String key : map.keySet()) {
            System.out.println(map.get(key));
        }

    }
}
