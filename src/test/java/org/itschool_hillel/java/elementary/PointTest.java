package org.itschool_hillel.java.elementary;

import org.junit.Assert;
import org.junit.Test;

public class PointTest {

    @Test
    public void testDistance() {
        double distance = new Point(1, 1).distance(new Point(0, 0));

        Assert.assertTrue(Math.abs(distance - Math.sqrt(2)) < 0.001);
    }
}
