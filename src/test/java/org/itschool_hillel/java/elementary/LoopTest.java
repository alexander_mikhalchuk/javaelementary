package org.itschool_hillel.java.elementary;

import org.junit.Assert;
import org.junit.Test;

public class LoopTest {

    @Test
    public void testWhile() {
        int i = 0;
        int x = 0;
        while (i <= 3) {
            x += i * i;
            i++;
        }

        Assert.assertEquals(14, x);
    }

    @Test
    public void testDoWhile() {
        int i = 0;
        int x = 0;
        do {
            x += i * i;
            i++;
        } while (i <= 3);

        Assert.assertEquals(14, x);
    }

    @Test
    public void testFor() {
        int y = 0;
        for (int i = 1, x = 0; i <= 3; i++) {
            x += i * i;
            y = x;
        }

        Assert.assertEquals(14, y);
    }

    @Test
    public void testContinue() {
        int x = 0;
        for (int i = 1; i <= 3; i++) {
            if (i == 2) continue;
            x += i * i;
        }

        Assert.assertEquals(10, x);
    }

    @Test
    public void testBreak() {
        int x = 0;
        for (int i = 1; i <= 3; i++) {
            x += i * i;
            if (i == 2) break;
        }

        Assert.assertEquals(5, x);
    }

    @Test
    public void testBreakWithLabel() {
        int x = 0;

        loop_i:
        for (int i = 1; i <= 3; i++) {
            for (int j = 1; j <= 3; j++) {
                x += i * j;
                if (i == 2) break loop_i;
            }
        }

        Assert.assertEquals(8, x);
    }

}
