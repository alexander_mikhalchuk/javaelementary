package org.itschool_hillel.java.elementary.stats;

import org.junit.Test;
import org.junit.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class CountStatCalculatorImplTest {

    private static final String VALUE1 = "first";
    private static final String VALUE2 = "second";
    private static final String VALUE3 = "third";


    // ТЕСТЫ ДЛЯ ОСНОВНОЙ ЧАСТИ ЗАДАНИЯ

    @Test
    public void testGetTwoMostFrequentEmptyList() {
        List<String> data = new ArrayList<String>();

        CountStatCalculatorImpl calculator = new CountStatCalculatorImpl();
        Set<String> result = calculator.getTwoMostFrequent(data);

        Assert.assertEquals(0, result.size());
    }

    @Test
    public void testGetTwoMostFrequentSingleElement() {
        List<String> data = new ArrayList<String>();
        data.add(VALUE1);

        CountStatCalculatorImpl calculator = new CountStatCalculatorImpl();
        Set<String> result = calculator.getTwoMostFrequent(data);

        Assert.assertEquals(1, result.size());
        Assert.assertTrue(result.contains(VALUE1));
    }

    @Test
    public void testGetTwoMostFrequentMultipleDifferentElements() {
        List<String> data = new ArrayList<String>();
        data.add(VALUE1);
        data.add(VALUE2);

        CountStatCalculatorImpl calculator = new CountStatCalculatorImpl();
        Set<String> result = calculator.getTwoMostFrequent(data);

        Assert.assertEquals(2, result.size());
        Assert.assertTrue(result.contains(VALUE1));
        Assert.assertTrue(result.contains(VALUE2));
    }

    @Test
    public void testGetTwoMostFrequentMultipleEqualElements() {
        List<String> data = new ArrayList<String>();
        data.add(VALUE1);
        data.add(VALUE1);

        CountStatCalculatorImpl calculator = new CountStatCalculatorImpl();
        Set<String> result = calculator.getTwoMostFrequent(data);

        Assert.assertEquals(1, result.size());
        Assert.assertTrue(result.contains(VALUE1));
    }

    @Test
    public void testGetTwoMostFrequent() {
        List<String> data = new ArrayList<String>();
        data.add(VALUE1);
        data.add(VALUE1);
        data.add(VALUE1);
        data.add(VALUE2);
        data.add(VALUE2);
        data.add(VALUE3);

        CountStatCalculatorImpl calculator = new CountStatCalculatorImpl();
        Set<String> result = calculator.getTwoMostFrequent(data);

        Assert.assertEquals(2, result.size());
        Assert.assertTrue(result.contains(VALUE1));
        Assert.assertTrue(result.contains(VALUE2));
        Assert.assertFalse(result.contains(VALUE3));
    }



    @Test
    public void testGetTwoLeastFrequentEmptyList() {
        List<String> data = new ArrayList<String>();

        CountStatCalculatorImpl calculator = new CountStatCalculatorImpl();
        Set<String> result = calculator.getTwoLeastFrequent(data);

        Assert.assertEquals(0, result.size());
    }

    @Test
    public void testGetTwoLeastFrequentSingleElement() {
        List<String> data = new ArrayList<String>();
        data.add(VALUE1);

        CountStatCalculatorImpl calculator = new CountStatCalculatorImpl();
        Set<String> result = calculator.getTwoLeastFrequent(data);

        Assert.assertEquals(1, result.size());
        Assert.assertTrue(result.contains(VALUE1));
    }

    @Test
    public void testGetTwoLeastFrequentMultipleDifferentElements() {
        List<String> data = new ArrayList<String>();
        data.add(VALUE1);
        data.add(VALUE2);

        CountStatCalculatorImpl calculator = new CountStatCalculatorImpl();
        Set<String> result = calculator.getTwoLeastFrequent(data);

        Assert.assertEquals(2, result.size());
        Assert.assertTrue(result.contains(VALUE1));
        Assert.assertTrue(result.contains(VALUE2));
    }

    @Test
    public void testGetTwoLeastFrequentMultipleEqualElements() {
        List<String> data = new ArrayList<String>();
        data.add(VALUE1);
        data.add(VALUE1);

        CountStatCalculatorImpl calculator = new CountStatCalculatorImpl();
        Set<String> result = calculator.getTwoLeastFrequent(data);

        Assert.assertEquals(2, result.size());
        Assert.assertTrue(result.contains(VALUE1));
        Assert.assertFalse(result.contains(VALUE2));
    }

    @Test
    public void testGetTwoLeastFrequent() {
        List<String> data = new ArrayList<String>();
        data.add(VALUE1);
        data.add(VALUE1);
        data.add(VALUE1);
        data.add(VALUE2);
        data.add(VALUE2);
        data.add(VALUE3);

        CountStatCalculatorImpl calculator = new CountStatCalculatorImpl();
        Set<String> result = calculator.getTwoLeastFrequent(data);

        Assert.assertEquals(2, result.size());
        Assert.assertFalse(result.contains(VALUE1));
        Assert.assertTrue(result.contains(VALUE2));
        Assert.assertTrue(result.contains(VALUE3));
    }



    // ТЕСТЫ ДЛЯ ДОПОЛНИТЕЛЬНОГО ЗАДАНИЯ

    @Test
    public void testGetTwoMostFrequentWithCollisions() {
        List<String> data = new ArrayList<String>();
        data.add(VALUE1);
        data.add(VALUE1);
        data.add(VALUE2);
        data.add(VALUE2);
        data.add(VALUE3);
        data.add(VALUE3);

        CountStatCalculatorImpl calculator = new CountStatCalculatorImpl();
        Set<String> result1 = calculator.getTwoMostFrequent(data);
        Set<String> result2 = calculator.getTwoMostFrequent(data);

        Assert.assertEquals(result1.contains(VALUE1), result2.contains(VALUE1));
        Assert.assertEquals(result1.contains(VALUE2), result2.contains(VALUE2));
        Assert.assertEquals(result1.contains(VALUE3), result2.contains(VALUE3));
    }

    @Test
    public void testGetTwoLeastFrequentWithCollisions() {
        List<String> data = new ArrayList<String>();
        data.add(VALUE1);
        data.add(VALUE1);
        data.add(VALUE2);
        data.add(VALUE2);
        data.add(VALUE3);
        data.add(VALUE3);

        CountStatCalculatorImpl calculator = new CountStatCalculatorImpl();
        Set<String> result1 = calculator.getTwoLeastFrequent(data);
        Set<String> result2 = calculator.getTwoLeastFrequent(data);

        Assert.assertEquals(result1.contains(VALUE1), result2.contains(VALUE1));
        Assert.assertEquals(result1.contains(VALUE2), result2.contains(VALUE2));
        Assert.assertEquals(result1.contains(VALUE3), result2.contains(VALUE3));
    }
}
