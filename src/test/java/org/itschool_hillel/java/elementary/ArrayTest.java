package org.itschool_hillel.java.elementary;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class ArrayTest {
    @Test
    public void testArray() {
        int[] a = new int[] {};
        assertEquals(0, a.length);

        assertEquals(3, new int[]{0, 1, 2}.length);

        int[] a1 = new int[] {0, 1, 2};
        assertEquals(1, a1[1]);
    }

    @Test
    public void testCopy() {
        char[] copyFrom = { 'd', 'e', 'c', 'a', 'f', 'f', 'e', 'i', 'n', 'a', 't', 'e', 'd' };
        char[] copyTo = new char[copyFrom.length];

        System.arraycopy(copyFrom, 0, copyTo, 0, copyFrom.length);
        assertArrayEquals(copyFrom, copyTo);
    }
}
