package org.itschool_hillel.java.elementary;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PrimitiveTypesTest {
    @Test
    public void testInt() {
        int four = 4;
        assertEquals(1, 1 + 0);
        assertEquals(3, 1 + 2);
        assertEquals(16, four * four);
        assertEquals(0, -1 + 1);
        assertEquals(Integer.MIN_VALUE, Integer.MAX_VALUE + 1);
    }

    @Test
    public void testByte() {
        byte zero = 0;
        assertEquals(Byte.MIN_VALUE, Byte.MIN_VALUE + zero);
    }
}
