package org.itschool_hillel.java.elementary.fasfood;

import org.itschool_hillel.java.elementary.fastfood.Dish;
import org.itschool_hillel.java.elementary.fastfood.Restaurant;
import org.itschool_hillel.java.elementary.fastfood.Table;
import org.itschool_hillel.java.elementary.fastfood.Waiter;
import org.itschool_hillel.java.elementary.people.Man;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

public class RestaurantTest {

    @Test
    public void testRestaurantBadMenu() {
        Restaurant reporter = new Restaurant();
        reporter.addDish(Dish.SHAURMA);
        reporter.addDish(Dish.VODKA);
        reporter.addTable(new Table());
        reporter.addWaiter(new Waiter(new Man("Игорь Шепель")));

        boolean result = reporter.serve(new Man("Chuck Norris"),
                Arrays.asList(new Dish[]{Dish.CHEESEBURGER, Dish.VODKA}));

        Assert.assertFalse(result);
    }


    @Test
    public void testRestaurant() {
        Restaurant reporter = new Restaurant();
        reporter.addDish(Dish.SHAURMA);
        reporter.addDish(Dish.VODKA);
        reporter.addTable(new Table());
        reporter.addWaiter(new Waiter(new Man("Игорь Шепель")));

        boolean result = reporter.serve(new Man("Chuck Norris"),
                Arrays.asList(new Dish[]{Dish.SHAURMA, Dish.VODKA}));

        Assert.assertTrue(result);
    }
}
