package org.itschool_hillel.java.elementary;

import org.junit.Assert;
import org.junit.Test;

public class InheritanceTest {

    public class Parent {
        private int field1;
        protected int field2;

        public Parent(int f1, int f2) {
            field1 = f1;
            field2 = f2;
        }

        public int sum() {
            return field1 + field2;
        }
    }

    public class Child extends Parent {
        public int field3;

        public Child(int f1, int f2, int f3) {
            super(f1, f2);
            field3 = f3;
        }

        @Override
        public int sum() {
            return super.sum() + field3;
        }
    }

    @Test
    public void testInheritance() {
        Parent p = new Parent(1, 2);
        Child c = new Child(1, 2, 3);

        Assert.assertEquals(3 , p.sum());
        Assert.assertTrue(p instanceof Parent);

        Assert.assertEquals(6 , c.sum());
        Assert.assertTrue(c instanceof Child);

        p = c;

        Assert.assertEquals(6 , p.sum());
        Assert.assertTrue(p instanceof Parent);
        Assert.assertTrue(p instanceof Child);

    }

}
